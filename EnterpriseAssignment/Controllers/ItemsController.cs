﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EnterpriseAssignment.Models;
using Microsoft.AspNet.Identity;
using PagedList;
using PagedList.Mvc;

namespace EnterpriseAssignment.Controllers
{
    [Authorize]
    public class ItemsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Items
        public ActionResult Index(int? p)
        {
            ViewBag.OwnerId = new SelectList(db.Users, "Id", "UserName");

            if (Session["OwnerId"] != null)
            {

                string ownerId = Session["OwnerId"].ToString();

                var items = db.Items.Include(i => i.ItemType).Include(i => i.Owner).Include(i => i.Quality).Where(x => x.OwnerId == ownerId);
                List<Item> itemsList = new List<Item>();
                itemsList = items.ToList();
                itemsList.Reverse();
                return View(itemsList.ToPagedList(p ?? 1, 6));
            }

            else
            {
                var items = db.Items.Include(i => i.ItemType).Include(i => i.Owner).Include(i => i.Quality);
                List<Item> itemsList = new List<Item>();
                itemsList = items.ToList();
                itemsList.Reverse();
                Session["OwnerId"] = null;
                return View(itemsList.ToPagedList(p ?? 1, 6));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string ownerId, int? p)
        {

            ViewBag.OwnerId = new SelectList(db.Users, "Id", "UserName");

            var sellerId = (from c in db.Items
                     where c.OwnerId == ownerId
                     select c.OwnerId).FirstOrDefault();

            if (sellerId != null)
            {
                
                var items = db.Items.Include(i => i.ItemType).Include(i => i.Owner).Include(i => i.Quality).Where(x => x.OwnerId == ownerId);
                List<Item> itemsList = new List<Item>();
                itemsList = items.ToList();
                itemsList.Reverse();
                Session["OwnerId"] = ownerId;
                return View(itemsList.ToPagedList(p ?? 1, 6));
            }

            else
            {
                var items = db.Items.Include(i => i.ItemType).Include(i => i.Owner).Include(i => i.Quality);
                List<Item> itemsList = new List<Item>();
                itemsList = items.ToList();
                itemsList.Reverse();
                Session["OwnerId"] = null;
                return View(itemsList.ToPagedList(p ?? 1, 6));
            }
        }

        // GET: Items/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // GET: Items/Create
        public ActionResult Create()
        {
            ViewBag.ItemTypeId = new SelectList(db.ItemTypes, "Id", "Name");
            //ViewBag.OwnerId = new SelectList(db.ApplicationUsers, "Id", "Email");
            ViewBag.QualityId = new SelectList(db.Qualities, "Id", "Name");
            return View();
        }

        // POST: Items/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Quantity,Price,OwnerId,QualityId,ItemTypeId")] Item item)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    item.OwnerId = User.Identity.GetUserId();
                    db.Items.Add(item);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                catch (Exception e)
                {
                    ViewBag.Duplicate = "Duplicate entries - This item is already registered with the same quality, quantity and price";
                }
            }

            ViewBag.ItemTypeId = new SelectList(db.ItemTypes, "Id", "Name", item.ItemTypeId);
            //ViewBag.OwnerId = new SelectList(db.ApplicationUsers, "Id", "Email", item.OwnerId);
            ViewBag.QualityId = new SelectList(db.Qualities, "Id", "Name", item.QualityId);
            return View(item);
        }

        // GET: Items/Edit/5
        public ActionResult Edit(long? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Items.Find(id);

            if (item == null)
            {
                return HttpNotFound();
            }

            if (item.OwnerId != User.Identity.GetUserId())
            {
                return RedirectToAction("Index");
            }

            ViewBag.ItemTypeId = new SelectList(db.ItemTypes, "Id", "Name", item.ItemTypeId);
            ViewBag.QualityId = new SelectList(db.Qualities, "Id", "Name", item.QualityId);
            return View(item);
        }

        // POST: Items/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Quantity,Price,OwnerId,QualityId,ItemTypeId")] Item item)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(item).State = EntityState.Modified;
                    item.OwnerId = User.Identity.GetUserId();
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                catch (Exception e)
                {
                    ViewBag.Duplicate = "Duplicate entries - This item is already registered with the same quality, quantity and price";
                }
            }
            ViewBag.ItemTypeId = new SelectList(db.ItemTypes, "Id", "Name", item.ItemTypeId);
            //ViewBag.OwnerId = new SelectList(db.ApplicationUsers, "Id", "Email", item.OwnerId);
            ViewBag.QualityId = new SelectList(db.Qualities, "Id", "Name", item.QualityId);
            return View(item);
        }

        // GET: Items/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }

            if (item.OwnerId != User.Identity.GetUserId())
            {
                return RedirectToAction("Index");
            }

            return View(item);
        }

        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {

            Item item = db.Items.Find(id);

            if (item.OwnerId != User.Identity.GetUserId())
            {
                return RedirectToAction("Index");
            }

            db.Items.Remove(item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
