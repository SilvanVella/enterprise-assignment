﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace EnterpriseAssignment
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        void Application_Error(object sender, EventArgs e)
        {
            HttpContext con = HttpContext.Current;
            var v = Server.GetLastError();

            var httpEx = v as HttpException;
            StringBuilder sb = new StringBuilder();

            string url = con.Request.Url.ToString();

            sb.AppendLine("Page:            " + con.Request.Url.ToString());
            sb.AppendLine("Error Message:            " + v.Message);

            if (v.InnerException != null)
            {
                sb.AppendLine("Inner Message:            " + v.InnerException.ToString());
            }

            string fileName = Path.Combine(Server.MapPath("~/Errors"), DateTime.Now.ToString("ddMMyyyyhhmmss") + ".txt");
            File.WriteAllText(fileName, sb.ToString());
        }
    }
}
