﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EnterpriseAssignment.Models
{
    [Table("Categories", Schema = "dbo")]
    public class Category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required(ErrorMessage ="The category name must be defined")]
        [StringLength(50, MinimumLength = 2, ErrorMessage ="The length of the category name must be between 2 and 50")]
        public string Name { get; set; }

        public virtual ICollection<ItemType> ItemTypes { get; set; }
    }
}