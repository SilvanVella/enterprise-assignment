﻿using EnterpriseAssignment.Models;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Download;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Web;

namespace GoogleDriveRestAPI_v3.Models
{
    public class GoogleDriveFilesRepository
    {
        //defined scope.
        public static string[] Scopes = { DriveService.Scope.Drive };

        //create Drive API service.
        public static DriveService GetService()
        {
            //get Credentials from credentials.json file 
            UserCredential credential;
            string credPath = HttpContext.Current.Server.MapPath(@"~/credentials.json");
            using (var stream = new FileStream(credPath, FileMode.Open, FileAccess.Read))
            {
                String FolderPath = HttpContext.Current.Server.MapPath("~");
                String FilePath = Path.Combine(FolderPath, "DriveServiceCredentials.json");

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(FilePath, true)).Result;
            }

            //create Drive API service.
            DriveService service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "EnterpriseAssigment",
            });
            return service;
        }

        //file Upload to the Google Drive.
        public static string FileUpload(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                DriveService service = GetService();
                string folderId = "1eEdJkNAFIsmjdbHPL0urMBfOJM0b2LHB";

                var FileMetaData = new Google.Apis.Drive.v3.Data.File()
                {
                    Name = file.FileName,
                    MimeType = MimeMapping.GetMimeMapping(file.FileName),
                    Parents = new List<string>
                    {
                        folderId
                    }
                };

                Google.Apis.Drive.v3.FilesResource.CreateMediaUpload request;

                Stream stream = file.InputStream;
                request = service.Files.Create(FileMetaData, stream , FileMetaData.MimeType);
                request.Fields = "id";
                request.Upload();
                
                var file1 = request.ResponseBody;
                return file1.Id;
            }

            return "";
        }
    }
}
