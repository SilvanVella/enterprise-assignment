﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EnterpriseAssignment.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public virtual ICollection<Item> Items { get; set; }
    }

    public class DbContextInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            base.Seed(context);

            List<Quality> qualities = new List<Quality>();

            qualities.Add(new Quality() { Name = "Excellent" });
            qualities.Add(new Quality() { Name = "Good" });
            qualities.Add(new Quality() { Name = "Poor" });
            qualities.Add(new Quality() { Name = "Bad" });

            context.Qualities.AddRange(qualities);
            context.Roles.AddOrUpdate(new IdentityRole { Id = "1", Name = "Admin" });
            context.Roles.AddOrUpdate(new IdentityRole { Id = "2", Name = "Registered User" });
            

            if (!(context.Users.Any(u => u.UserName == "admin@admin.com")))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var userToInsert = new ApplicationUser { UserName = "admin@admin.com", Email = "admin@admin.com" };
                userManager.Create(userToInsert, "Password@123");
                userManager.AddToRole(userToInsert.Id, "Admin");
            }

            addTestUsers(context, 10);
            addTestCategories(context, 10);
            addTestItemTypes(context, 15);
            addTestItems(context, 100);
        }

        private void addTestUsers(ApplicationDbContext context, int max)
        {
            for (int i = 0; i < max; i++)
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var userToInsert = new ApplicationUser { UserName = "testUser" + i + "@test.com", Email = "testUser" + i + "@test.com" };
                userManager.Create(userToInsert, "Test@123");
                userManager.AddToRole(userToInsert.Id, "Registered User");
            }
        }

        private void addTestCategories(ApplicationDbContext context, int max)
        {
            List<Category> categories = new List<Category>();

            for (int i = 0; i < max; i++)
            {
                categories.Add(new Category() { Name = "Category" + i });
            }

            context.Categories.AddRange(categories);
            context.SaveChanges();
        }

        private void addTestItemTypes(ApplicationDbContext context, int max)
        {
            List<ItemType> itemTypes = new List<ItemType>();
            List<Category> categories = context.Categories.ToList();
            int catIndex = 0;

            if (categories.Count > 0)
            {
                for (int i = 0; i < max; i++)
                {

                    if (catIndex == categories.Count)
                    {
                        catIndex = 0;
                    }

                    itemTypes.Add(new ItemType() { Name = "ItemType" + i, Image = "1wRJehrTIKo4bl3dPPkhGRN_dyGoTDPGb",  CategoryId = categories[catIndex].Id});
                    catIndex++;
                }

                context.ItemTypes.AddRange(itemTypes);
                context.SaveChanges();
            }
        }

        private void addTestItems(ApplicationDbContext context, int max)
        {
            List<Item> items = new List<Item>();
            List<ItemType> itemTypes = context.ItemTypes.ToList();
            List<ApplicationUser> users = context.Users.ToList();
            List<Quality> qualities = context.Qualities.ToList();
            int itemTypesIndex = 0;
            int usersIndex = 0;
            int qualitiesIndex = 0;

            if (itemTypes.Count > 0 && users.Count > 0 && qualities.Count > 0)
            {
                for (int i = 1; i <= max; i++)
                {
                    if (itemTypesIndex == itemTypes.Count)
                    {
                        itemTypesIndex = 0;
                    }
                    if (usersIndex == users.Count)
                    {
                        usersIndex = 0;
                    }
                    if (qualitiesIndex == qualities.Count)
                    {
                        qualitiesIndex = 0;
                    }
                    items.Add(new Item() { Quantity = i, Price = i, OwnerId = users[usersIndex].Id, QualityId = qualities[qualitiesIndex].Id,
                        ItemTypeId = itemTypes[itemTypesIndex].Id });
                    itemTypesIndex++;
                    usersIndex++;
                    qualitiesIndex++;
                }
                context.Items.AddRange(items);
                context.SaveChanges();
            }
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("EnterpriseConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer(new DbContextInitializer());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<EnterpriseAssignment.Models.Category> Categories { get; set; }

        public System.Data.Entity.DbSet<EnterpriseAssignment.Models.ItemType> ItemTypes { get; set; }

        public System.Data.Entity.DbSet<EnterpriseAssignment.Models.Item> Items { get; set; }

        public System.Data.Entity.DbSet<EnterpriseAssignment.Models.Quality> Qualities { get; set; }
        public IEnumerable ApplicationUsers { get; internal set; }
    }
}