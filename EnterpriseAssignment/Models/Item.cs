﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EnterpriseAssignment.Models
{
    public class Item
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required(ErrorMessage = "The quantity must be defined")]
        [Index("UN_OwnerId_Quality_Price", 4, IsUnique = true)]
        [Range(1, int.MaxValue, ErrorMessage = "Only positive numbers allowed")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "The price must be defined")]
        [Range(1, int.MaxValue, ErrorMessage = "Only positive numbers allowed")]
        [Index("UN_OwnerId_Quality_Price", 1, IsUnique = true)]
        public float Price { get; set; }

        [Index("UN_OwnerId_Quality_Price", 2, IsUnique = true)]
        public string OwnerId { get; set; }
        public virtual ApplicationUser Owner { get; set; }

        [Required(ErrorMessage = "The quality name must be defined")]
        [Index("UN_OwnerId_Quality_Price", 3, IsUnique = true)]
        [Display(Name = "Quality")]
        public long QualityId { get; set; }
        public Quality Quality { get; set; }

        [Required(ErrorMessage = "An Item Type must be selected")]
        [Display(Name = "Item Type")]
        public long ItemTypeId { get; set; }
        public ItemType ItemType { get; set; }
    }
}