﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EnterpriseAssignment.Models
{
    public class ItemType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required(ErrorMessage = "The ItemType name must be defined")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "The length of the category name must be between 2 and 50")]
        public string Name { get; set; }

        public string Image { get; set; }

        [Required(ErrorMessage = "A category must be selected")]
        [Display(Name = "Category")]
        public long CategoryId { get; set; }
        public Category Category { get; set; }

        public virtual ICollection<Item> Items { get; set; }
    }
}